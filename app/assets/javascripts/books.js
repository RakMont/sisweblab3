
$(function() {
    $('.edit_book input[type=submit]').remove();
    $('.edit_book input[type=checkbox]').click(function() {
        $(this).parent('form').submit();
    });
        return this;
});

jQuery.fn.submitOnCheck = function() {
    this.find('input[type=submit]').remove();
    this.find('input[type=checkbox]').click(function() {
        $(this).parent('form').submit();
    });
    return this;
}

$(function() {
    $('.edit_book').submitOnCheck();
});